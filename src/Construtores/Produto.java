/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import java.io.Serializable;

/**
 *
 * @author carlo
 */
public abstract class Produto implements Serializable{
    private static final long serialVersionUID = 3L;
    protected String codigo;
    protected String descricao;
    protected float valor;
    
    public Produto(String codigo, String descricao, float valor){
        this.codigo=codigo;
        this.descricao=descricao;
        this.valor=valor;
    }
    
    public void setCodigo(String codigo){
        this.codigo=codigo;
    }
    public String getCodigo(){
        return codigo;
    }
    public void setDescricao(String descricao){
        this.descricao = descricao;
    }
    public String getDescricao(){
        return descricao;
    }
    public void setValor(float valor){
        this.valor=valor;
    }
    public float getValor(){
        return valor;
    }
    
    public String toString(){
        return codigo +""+descricao;
    }
    
    public abstract float calcularPreco();
    public abstract String relatorio();
    public abstract void exibir();
    
    
}
