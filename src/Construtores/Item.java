/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import java.io.Serializable;
import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class Item implements Serializable {
    private static final long serialVersionUID = 2L;
    private int num;
    private String codigo;
    private String descricao;
    private float valor;
    private float quantidade;
    
    public Item(int num, String codigo, String descricao, float valor, float quantidade){
        this.num = num;
        this.codigo = codigo;
        this.descricao = descricao;
        this.valor = valor;
        this.quantidade = quantidade;
    }
    public void setNum(int num){
        this.num = num;
    }
    
    public int getNum(){
        return num;
    }
    
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
    
    public String getCodigo(){
        return codigo;
    }
    
    public void setDescricao(String descricao){
        this.descricao = descricao;
    }
    
    public String getDescricao(){
        return descricao;
    }
    
    public void setValor(float valor){
        this.valor = valor;
    }
    
    public float getValor(){
        return valor;
    }
    
    public void setQuantidade(float quantidade){
        this.quantidade = quantidade;
    }
    
    public float getQuantidade(){
        return quantidade;
    }

    public float calcularTotal(){
        return valor * quantidade;
    }
    
    public String relatorio(){
        String msg;
        msg = num + " - ";
        msg = msg + " "+descricao;
        msg = msg + "\nCodigo : "+codigo;
        msg = msg + "\nValor : "+valor;
        msg = msg + "\nQuantidade : "+quantidade;
        msg = msg + "\nTotal : "+calcularTotal();
        
        return msg;
    }
    
    public void exibir(){
        JOptionPane.showMessageDialog(null, relatorio());
    }
    
}
