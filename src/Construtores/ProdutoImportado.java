/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class ProdutoImportado extends Produto{
    private float taxaImposto;
    private float taxaImportacao;
    
    public ProdutoImportado(String codigo, String descricao, float valor, float taxaImposto, float taxaImportacao){
        super(codigo, descricao, valor);
        this.taxaImposto = taxaImposto;
        this.taxaImportacao = taxaImportacao;
    }
    @Override
    public float calcularPreco(){
        return valor+valor*taxaImposto+valor*taxaImportacao;
    }
    @Override
    public String relatorio(){
        String msg;
        msg = "Código :"+codigo;
        msg = msg + "\nDescrição : "+descricao;
        msg = msg + "\n Valor : "+valor;
        msg = msg + "\n Taxa Imposto : "+taxaImposto;
        msg = msg + "\n Taxa Importacao : "+taxaImportacao;
        msg = msg + "\n Preço: "+calcularPreco();
        
        return msg;
    }
    @Override
    public void exibir(){
        JOptionPane.showMessageDialog(null,"\n Produto Importado :"+relatorio());
    }
    
}
