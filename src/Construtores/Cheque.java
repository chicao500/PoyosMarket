/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class Cheque extends TipoPagamento{
    private String nomeEmissor;
    private String numeroCheque;
    
    public Cheque(String nomeEmissor, String numeroCheque){
        super("Cheque");
        this.nomeEmissor = nomeEmissor;
        this.numeroCheque = numeroCheque;
    }
    public void setNomeEmissor(String nomeEmissor){
        this.nomeEmissor = nomeEmissor;
    }
    public String getNomeEmissor(){
        return nomeEmissor;
    }
    public void setNumeroCheque(String numeroCheque){
        this.numeroCheque = numeroCheque;
    }
    public String getNumeroCheque(){
        return numeroCheque;
    }
    
    @Override
    public String relatorio(){
        String msg;
        msg = "Tipo de Pagamento : "+tipoPagamento;
        msg = msg +"\nNome Emissor : "+nomeEmissor;
        msg = msg +"nNumero Cheque : "+numeroCheque;
        
        return msg;
    }
    @Override
    public void exibirDados(){
        JOptionPane.showMessageDialog(null, relatorio());
    }
}
