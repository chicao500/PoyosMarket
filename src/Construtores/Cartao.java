/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class Cartao extends TipoPagamento {
    private String nome;
    private String numero;
    
    public Cartao(String nome, String numero){
        super("Cartao");
        this.nome = nome;
        this.numero = numero;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    public String getNome(){
        return nome;
    }
    public void setNumero(String numero){
        this.numero = numero;
    }
    public String getNumero(){
        return numero;
    }
    @Override
    public String relatorio(){
        String msg;
        msg = "Tipo de Pagamento: "+tipoPagamento;
        msg = msg + "\nNome : "+nome;
        msg = msg + "\nNumero : "+numero;
        
        return msg;
    }
    @Override
    public void exibirDados(){
        JOptionPane.showMessageDialog(null, relatorio());
    }
    
}
