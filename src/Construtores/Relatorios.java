/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author carlo
 */
public class Relatorios extends JDialog{
    private JTextArea relatorioTArea; //Componente para exibição de texto
    private JScrollPane sPane;        //Pane para implementar barra de rolagem
    
    public Relatorios(String mensagem){
        relatorioTArea = new JTextArea();//instanciação do objetO
        relatorioTArea.append(mensagem);//adicionado string ao objeto de texto
        //Instanciação do objeto, passando o componente de texto pelo construtor para criar uma associação
        sPane = new JScrollPane(relatorioTArea);
        add(sPane);//Adicionado o pane de rolagem na janela do JDialog
    }
    public void exibir(int largura, int altura){
        this.setModal(true);//true para bloequear a execução e continuar quando fechar
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);//destruir o objeto quando fechar
        setSize(largura,altura);//define o tamanho da janela
        setVisible(true);//exibe a janela
    }
    
}
