/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class ProdutoNacional extends Produto{
    private float taxaImposto;
   
    public ProdutoNacional(String codigo, String descricao, float valor, float taxaImposto){
        super(codigo, descricao, valor);
        this.taxaImposto = taxaImposto;
    }
    
    @Override
    public float calcularPreco(){
        return valor + valor*taxaImposto; 
    }
    @Override
    public String relatorio(){
        String msg;
        msg = "Codigo : "+codigo;
        msg = msg + "\n Descricao : "+descricao;
        msg = msg + "\n Valor : "+valor;
        msg = msg + "\n Taxa Imposto : "+taxaImposto;
        msg = msg + "\n Preço: "+calcularPreco();
        
        return msg;
    }
    @Override
    public void exibir(){
        JOptionPane.showMessageDialog(null, "\n Produto Nacional"+relatorio());
    }
    
}
