/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import java.io.Serializable;
import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class Vendas implements Serializable {
    private static final long serialVersionUID = 5L;
    private String numero;
    private Item[] itens;
    private TipoPagamento tipoPgto;
    private Cliente cliente;
    private Calendar data = Calendar.getInstance();
    private int cont;
    
    public Vendas(String numero, TipoPagamento tipoPgto, Cliente cliente){
        this.numero = numero;
        this.tipoPgto = tipoPgto;
        this.cliente = cliente;
        this.itens = new Item [20];
        this.cont = 0;        
    }
    
    public String getCliente(){
        return this.cliente.getCpf();
    }
    public Cliente getCliente1(){
        return this.cliente;
    }
    
    public TipoPagamento getTipoPgto(){
        return this.tipoPgto;
    }
    
    public String getPagamento(){
        return this.tipoPgto.tipoPagamento;
    }
    
    public String getNumero(){
        return this.numero;
    }
    
    public void addItem(Item item){
        if (cont<20){
            itens[cont]=item;
            cont++;
        }
    }
    
    public float calcularTotal(){
        float soma = 0;
        for (int i=0; i<cont; i++){
            soma += itens[i].calcularTotal();
        }
        return soma;
    }
    
    public String getData(){
        int dia = data.get(Calendar.DAY_OF_MONTH);
        int mes = data.get(Calendar.MONTH);
        int ano = data.get(Calendar.YEAR);
        
        String d = " "+dia+"/"+mes+"/"+ano;
        return d;
    }
    public String relatorio(){
        String msg;
        msg = "Numero de Venda : "+numero;
        msg = msg +"\n Data : "+getData();
        msg = msg +"\n"+cliente.relatorio();
        msg = msg +"\n"+cont+"Itens :";
        for(int i =0; i<cont ; i++){
            msg = msg + "\n"+itens[i].relatorio();
        }
        msg = msg + "\n\n Total : "+calcularTotal();
        msg = msg + "\n"+tipoPgto.relatorio();
        
        return msg;
    }
    public void exibir(){
        JOptionPane.showMessageDialog(null, "Venda \n "+relatorio());
    }
}
