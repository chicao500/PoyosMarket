/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Construtores;

import java.io.Serializable;
import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class Cliente implements Serializable {
    private static final long serialVersionUID = 1L;
    private String nome;
    private String cpf;
    
    public Cliente(String nome, String cpf){
        this.nome = nome;
        this.cpf = cpf;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    public String getNome(){
        return nome;
    }
    public void setCpf(String cpf){
        this.cpf = cpf;
    }
    public String getCpf(){
        return cpf;
    }
    
    public String relatorio(){
        String msg;
        msg = "Nome : "+nome;
        msg = msg + "\n CPF : "+cpf;
        
        return msg;
    }
    
    public void exibir(){
        JOptionPane.showMessageDialog(null, "CLIENTE"+
                                      "\nNome: "+nome+
                                      "\nCPF : "+cpf);
    }
    
}
