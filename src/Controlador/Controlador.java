/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import BancodeDados.BDClientes;
import BancodeDados.BDProdutos;
import BancodeDados.BDVendas;
import Construtores.Cartao;
import Construtores.Cheque;
import Construtores.Cliente;
import Construtores.Dinheiro;
import Construtores.Item;
import Construtores.Produto;
import Construtores.ProdutoImportado;
import Construtores.ProdutoNacional;
import Construtores.Relatorios;
import Construtores.TipoPagamento;
import Construtores.Vendas;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;

/**
 *
 * @author carlo
 */
public class Controlador {
    private static int cont1, cont2, cont3, contItens;
    
    public int getCont3(){
        return cont3;
    }
    
    public void cadastrarClientes(String nome, String cpf){
        if(cont1<100){
            BDClientes.clientes[cont1] = new Cliente (nome,cpf);
            cont1++;
        }
        else JOptionPane.showMessageDialog(null, "Não há mais espaço");
        
    }
    
    public void relatorioClientes(){
        String msg = null;
        for(int i=0;i<cont1;i++){
            msg = msg + "\n\n"+BDClientes.clientes[i].relatorio();
        }
        Relatorios rel = new Relatorios(msg);
        rel.exibir(700,400);
        
    }
    
    public Cliente buscarCliente (String cpf){
        for(int i=0;i<cont1;i++){
            if(BDClientes.clientes[i].getCpf().equals(cpf)){
                return(BDClientes.clientes[i]);
            }
        }
        return null;
    }
    
    public void relatorioClienteEspecífico(String cpf){
        if(buscarCliente(cpf) != null){
            Cliente aux = buscarCliente (cpf);
            aux.exibir();
        }
    }
    
    public void relatorioGastoCliente(String cpf){
        float total = 0.0f;
        String msg = null;
        
        for(int i=0;i<cont2;i++){
            if(BDVendas.vendas[i].getCliente().equals(cpf))
                msg = msg + "\n\n" +BDVendas.vendas[i].relatorio();
                total += BDVendas.vendas[i].calcularTotal();
        }
        msg = msg + "\n Total Gasto : "+total;
        Relatorios rel = new Relatorios(msg);
        rel.exibir(700,400);
    }
    
    public void cadastrarProdutoNacional(String codigo, String descricao, float valor, 
                                                                    float taxadeImposto){
        if(cont3<100){
            BDProdutos.produtos[cont3]=new ProdutoNacional (codigo, descricao, valor, taxadeImposto);
            cont3++;
        }
    }
    
    public void cadastrarProdutoImportado(String codigo, String descricao, float valor, 
                                            float taxadeImposto, float taxadeImportacao){
        if(cont3<100){
            BDProdutos.produtos[cont3] = new ProdutoImportado(codigo, descricao, valor, taxadeImposto, taxadeImportacao);
            cont3++;
        }
    
    }
    
    public void relatorioProdutoGeral(){
        String msg = null;
        for(int i =0;i<cont3;i++){
            msg = msg + "\n\n "+BDProdutos.produtos[i].relatorio();
        }
        Relatorios rel = new Relatorios (msg);
        rel.exibir(700,400);
    }
    
    public void relatorioProdutoEspecífico(String codigo){
        for(int i =0;i<cont3;i++){
            if(BDProdutos.produtos[i].getCodigo().equals(codigo)){
                BDProdutos.produtos[i].exibir();
                return;            
            }
        }
    }
    
    public TipoPagamento tipoPagamento(String tipoPgto){
        if(tipoPgto.equals("Dinheiro")){
            TipoPagamento novo = new Dinheiro();
            return novo;
        }else if(tipoPgto.equals("Cartao")){
            String nome = JOptionPane.showInputDialog("Insira o nome do proprietário do cartão : ");
            String numero = JOptionPane.showInputDialog("Insinara o numero do usuário do cartão : ");
            TipoPagamento novo = new Cartao(nome,numero);
            return novo;
        }else if(tipoPgto.equals("Cheque")){
            String nomeE= JOptionPane.showInputDialog("Insira o nome do emissor do cheque : ");
            String numeroC= JOptionPane.showInputDialog("Insira o numero do cheque : ");
            TipoPagamento novo = new Cheque (nomeE, numeroC);
            return novo;
        }
        return null;
    }
    
    public Produto buscarProduto (String codigo){
        for(int i =0; i<cont3;i++){
            if(BDProdutos.produtos[i].getCodigo().equals(codigo)){
                return BDProdutos.produtos[i];
            }
        }
        JOptionPane.showMessageDialog(null, "Produto não cadastrado !");
        return null;
    }
    
    public void adicionarItens (int aux, Produto p, float quantidade){
        contItens++;
        Item item = new Item(contItens,p.getCodigo(),p.getDescricao(),p.calcularPreco(),quantidade);
        BDVendas.vendas[aux].addItem(item);
    }
    
    public int registrarCompra(String cpf, String tipoPagamento){
        int aux2=0;
        TipoPagamento tipoPgto;
        if(cont2<100){
            if(buscarCliente (cpf)==null){
                JOptionPane.showMessageDialog(null, "Cliente não encontrado !\n "
                        + "Por favor, retorne ao menu principal");
            }else if ((tipoPgto = tipoPagamento (tipoPagamento))==null){
                JOptionPane.showMessageDialog(null, "Tipo de Pagamento inexistente ! \n" 
                        + "Por favor, retorno ao menu principal");
            }else{
                Cliente cliente = buscarCliente(cpf);
                BDVendas.vendas[cont2] = new Vendas (Integer.toString(cont2),tipoPgto, cliente);
            }
        }
        return aux2;
    }
    
    public void relatorioVendaGeral(){
        String msg = null;
        for(int i =0;i<cont2;i++){
            msg = msg + "\n\n"+BDVendas.vendas[i].relatorio();
        }
        Relatorios rel = new Relatorios(msg);
        rel.exibir(700,400);
    }
    
    public void relatorioVendaEspecifico(String numero){
        for(int i =0;i<cont2;i++){
            if(BDVendas.vendas[i].getNumero().equals(numero)){
                BDVendas.vendas[i].exibir();
                return;
            }
        }
    }
    
    public void relatorioVendaS(String tipoPgto){
        String msg = null;
        for(int i =0;i<cont2;i++){
            if(BDVendas.vendas[i].getPagamento().contentEquals(tipoPgto)){
                
                msg= msg + "\n\n"+BDVendas.vendas[i].getCliente1().relatorio();
                msg= msg + "\n Total : "+BDVendas.vendas[i].calcularTotal();
                msg= msg + "\n"+BDVendas.vendas[i].getData();
                msg= msg + "\n"+BDVendas.vendas[i].getTipoPgto().relatorio();
            }
        }
        Relatorios rel = new Relatorios(msg);
        rel.exibir(700,400);
    }
    
    public void relatorioVendaD(String tipoPgto){
        String msg = null;
        for(int i=0;i<cont2;i++){
            if(BDVendas.vendas[i].getPagamento().contentEquals(tipoPgto)){
                msg = msg + "\n\n" + BDVendas.vendas[i].relatorio();
            }
            
        }
        Relatorios rel = new Relatorios (msg);
        rel.exibir(700,400);
    }
    
    public void salvarDados1() throws FileNotFoundException , IOException{
        FileOutputStream fos = new FileOutputStream("Cliente.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        
        oos.writeInt(cont1);
        for(int i =0;i<BDClientes.clientes.length;i++){
            oos.writeObject(BDClientes.clientes[i]);
        }
    }
    
    public void carregarDados1() throws FileNotFoundException, IOException, ClassNotFoundException{
        FileInputStream fis = new FileInputStream("Cliente.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        cont1 = ois.readInt();
        for(int i =0; i<cont1; i++){
            BDClientes.clientes[i] = (Cliente)ois.readObject();
        }//Lê-se um objeto
    }
    
    public void salvarDados2() throws FileNotFoundException, IOException{
        FileOutputStream fos = new FileOutputStream("Produto.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeInt(cont3);
        for(int i =0; i<BDProdutos.produtos.length;i++){
            oos.writeObject(BDProdutos.produtos[i]);
        }
    }
    public void carregarDados2() throws FileNotFoundException, IOException, ClassNotFoundException{
        FileInputStream fis = new FileInputStream("Produto.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        cont3 = ois.readInt();
        for(int i =0;i<cont3;i++){
            BDProdutos.produtos[i] = (Produto) ois.readObject();
        }//lê-se um objeto
    }
    
    public void salvarDados3() throws FileNotFoundException, IOException{
        FileOutputStream fos = new FileOutputStream("Vendas.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        
        oos.writeInt(cont2);
        for(int i =0; i<BDVendas.vendas.length;i++){
            oos.writeObject(BDVendas.vendas[i]);
        }
    }
    
    public void carregarDados3() throws FileNotFoundException, IOException, ClassNotFoundException{
        FileInputStream fis = new FileInputStream("Venda.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        
        cont2= ois.readInt();
        for(int i=0; i<cont2;i++){
            BDVendas.vendas[i] = (Vendas)ois.readObject();
        }//lê-se um objeto
    }
}

